SQLite Diff Script
=========

Introduction
------------

This script takes two SQLite tables with the same structure and provides 
list of differences - entries that have been removed or added and per 
column changes for each entry.

One of the possible uses - you have two copies of the same table in different 
points of time and want to have an audit log of all the changes. 

The same approach can be used for finding differences in pretty much 
any tabular data structures, like large CSV or Excel files (with the files 
being imported into SQLite first)   


Usage
-----


Example
--------

......



TODO
----

- Finish this README file.
