def diffs(db_path, left_orig, right_orig, key_lst, test_lst = None):
    import sqlite3
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()
    
    key_val = " || ".join(key_lst)
    
    cursor.execute("drop table if exists diff_left")
    conn.commit()

    cursor.execute("create table diff_left as select " + left_orig + ".*, (" + " || ".join(key_lst) + ") as diff_id from " + left_orig)
    conn.commit()

    cursor.execute("drop table if exists diff_right")
    conn.commit()
    
    cursor.execute("create table diff_right as select " + right_orig + ".*, (" + " || ".join(key_lst) + ") as diff_id from " + right_orig)
    conn.commit()

    
############################################
#### for subj off only start

    cursor.execute("delete from diff_left where excl_fr_all_fg = 'Y'")
    conn.commit()
    
    cursor.execute("delete from diff_right where excl_fr_all_fg = 'Y'")
    conn.commit()

############################################
#### for subj off only end

    
    cursor.execute("create index diff_left_ix_01 on diff_left(diff_id)")
    conn.commit()
    
    cursor.execute("create index diff_right_ix_01 on diff_right(diff_id)")
    conn.commit()

    if test_lst == None:
        cursor.execute("PRAGMA table_info(" + left_orig + ")")
        col_names = list(map(lambda x: x[1].lower(), cursor.fetchall()))
        exclude = key_lst
        exclude.append("diff_id")
        test_lst = [x.lower() for x in col_names if x not in exclude]

    
# diff_audit_log start ###########    

    cursor.execute("drop table if exists diff_audit_log")
    conn.commit()

    cursor.execute("create table diff_audit_log (diff_id text, new_or_removed text, field text, change text)")
    conn.commit()
    
    cursor.execute("insert into diff_audit_log (diff_id, new_or_removed) select distinct diff_id, 'removed' from diff_left where diff_id not in (select diff_id from diff_right)")
    conn.commit()
    
    cursor.execute("insert into diff_audit_log (diff_id, new_or_removed) select distinct diff_id, 'new' from diff_right where diff_id not in (select diff_id from diff_left)")
    conn.commit()
   
    sql_draft = """  insert into diff_audit_log (diff_id, field, change) 
                     select diff_left.diff_id
                            ,'&&field'
                            ,ifnull(diff_left.&&field,'') || '  -->  ' || ifnull(diff_right.&&field,'') 
                     from diff_left
                     inner join diff_right 
                     on diff_right.diff_id = diff_left.diff_id 
                     where ifnull(diff_right.&&field, '') <> ifnull(diff_left.&&field, '')"""
    
    for field in test_lst:
        sql = sql_draft.replace("&&field", field)
        cursor.execute(sql)
    conn.commit()
    
# diff_audit_log end ########### 

   
# diff_audit_tb start ########### 
    
    cr_audit_tb = " text, ".join(test_lst)
    in_audit_tb = ", ".join(test_lst)
    
    cursor.execute("drop table if exists diff_audit_tb")
    conn.commit()
    
    cursor.execute("create table diff_audit_tb (diff_id text, new_or_removed text, " + cr_audit_tb + ")")
    conn.commit()
    
    cursor.execute("insert into diff_audit_tb (diff_id, new_or_removed) select distinct diff_id, 'removed' from diff_left where diff_id not in (select diff_id from diff_right)")
    conn.commit()
    
    cursor.execute("insert into diff_audit_tb (diff_id, new_or_removed) select distinct diff_id, 'new' from diff_right where diff_id not in (select diff_id from diff_left)")
    conn.commit()
    
    
    sql1_draft = """ update diff_audit_tb
                     set &&field = (
                                     select ifnull(diff_left.&&field,'') || '  -->  ' || ifnull(diff_right.&&field,'') 
                                     from diff_left
                                     inner join diff_right on diff_right.diff_id = diff_left.diff_id 
                                          and ifnull(diff_right.&&field,'') <> ifnull(diff_left.&&field,'')
                                     where diff_left.diff_id = diff_audit_tb.diff_id
                                    )"""
    
    
    sql2_draft = """ insert into diff_audit_tb (diff_id, &&field) 
                     select distinct diff_left.diff_id
                            ,ifnull(diff_left.&&field,'') || '  -->  ' || ifnull(diff_right.&&field,'') 
                     from diff_left
                     inner join diff_right on diff_right.diff_id = diff_left.diff_id 
                     where ifnull(diff_right.&&field, '') <> ifnull(diff_left.&&field,'')
                        and not exists (
                                         select null
                                         from diff_audit_tb
                                         where diff_id = diff_left.diff_id
                                        )"""
    
    
    for field in test_lst:
        sql = sql1_draft.replace("&&field", field)
        cursor.execute(sql)
        sql = sql2_draft.replace("&&field", field)
        cursor.execute(sql)
    conn.commit()

     
    
    ############################################
    #### for subj off only start
    cursor.execute("drop table if exists diff_" + left_orig + "_to_" + right_orig)
    conn.commit()
    
    sql = "CREATE table diff_" + left_orig + "_to_" + right_orig + \
          """ AS
            SELECT diff_left.spk_cd
                  ,diff_left.spk_ver_no
                  ,diff_left.spk_title
                  ,diff_left.sprd_desc
                  ,diff_left.location
                  ,diff_left.avail_desc
                  ,diff_audit_tb.*
            FROM diff_audit_tb
            INNER JOIN diff_left ON diff_audit_tb.diff_id = diff_left.diff_id
                AND diff_left.excl_fr_all_fg <> 'Y'
            
            UNION
            
            SELECT diff_right.spk_cd
                  ,diff_right.spk_ver_no
                  ,diff_right.spk_title
                  ,diff_right.sprd_desc
                  ,diff_right.location
                  ,diff_right.avail_desc
                  ,diff_audit_tb.*
            FROM diff_audit_tb
            INNER JOIN diff_right ON diff_audit_tb.diff_id = diff_right.diff_id
                AND diff_right.excl_fr_all_fg <> 'Y'
                AND diff_audit_tb.new_or_removed = 'new'
            ORDER BY spk_cd
                ,spk_ver_no
                ,sprd_desc
                ,location
                ,avail_desc"""

    cursor.execute(sql)
    conn.commit()

    ############################################
    #### for subj off only end



    
    cursor.execute("drop table if exists diff_left")
    cursor.execute("drop table if exists diff_right")
    conn.commit()
    
    conn.execute("VACUUM")
    conn.close()



# example for subj_offerings
# cust_lst = ['spk_title', 'teaching_census', 'eftsl', 'csp_cd', 'sfb_cd', 'hecs_amt','au_fee_amt', 'os_fee_amt', 'cap_asses_amt', 'cap_non_asses_amt']
# diffs("data/subj_off_2014.sqlite", "publ_20131108", "publ_20131120", ["spk_cd","spk_ver_no","avail_yr","sprd_cd","location_cd","avail_no"], cust_lst)
